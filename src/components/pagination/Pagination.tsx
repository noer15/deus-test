import React, { useEffect } from "react";
import classnames from "clsx";
import { usePagination, DOTS } from "./usePagination";
import style from "./Pagination.module.css";
import { useRouter, usePathname, useSearchParams } from "next/navigation";
import { Button } from "@/components/ui/button";
import { RxDoubleArrowRight, RxDoubleArrowLeft } from "react-icons/rx";

interface PropsPaginate {
  onPageChange?: (...args: any[]) => any;
  totalCount?: number | any;
  siblingCount?: number;
  currentPage?: any;
  pageSize?: number;
  className?: string;
  label?: any;
}
const PaginationPage: React.FC<PropsPaginate> = ({
  onPageChange,
  totalCount,
  siblingCount = 1,
  currentPage,
  pageSize,
  label,
  className,
}: any) => {
  const router = useRouter();
  const pathname = usePathname();
  const paginationRange: any = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  if (typeof window !== "undefined") {
    const searchParams = new URLSearchParams(window.location.search);
    const pageQuery = searchParams.get("page");
  }

  const navigateToPage = (newPage: any) => {
    const currentPath = window.location.pathname;
    const currentSearch = window.location.search;
    const currentQuery = new URLSearchParams(currentSearch);

    // Menggabungkan query parameters yang ada dengan yang baru
    currentQuery.set("page", newPage);
    onPageChange(newPage);

    // Memeriksa apakah query parameters sudah ada
    const updatedQuery = currentQuery.toString()
      ? `?${currentQuery.toString()}`
      : "";

    // Menggabungkan path dan query parameters dengan & jika perlu
    const updatedURL = `${currentPath}${updatedQuery}`;

    // Menggantikan URL dengan replaceState
    window.history.replaceState({}, "", updatedURL);
  };

  const onNext = () => {
    navigateToPage(currentPage + 1);
  };

  const onPrevious = () => {
    navigateToPage(currentPage - 1);
  };

  const onPageChangeWithRouter = (newPage: number) => {
    navigateToPage(newPage);
  };

  let lastPage = paginationRange[paginationRange.length - 1];

  const isPaginationItemActive = (pageNumber: number) => {
    if (typeof window !== "undefined") {
      const searchParams = new URLSearchParams(window.location.search);
      const pageQuery = searchParams.get("page");
      const pageParam = pageQuery ? parseInt(pageQuery, 10) : 1;
      return pageParam === pageNumber;
    }
  };

  return (
    <div className="flex justify-between items-center">
      <span className="text-gray-500 hidden md:block lg:block xl:block text-sm py-2">
        Showing {label?.current_page ?? 1} to {label?.per_page ?? 10} of{" "}
        {label?.total ?? 0} entries
      </span>
      <ul
        className={classnames(style.pagination_container, {
          [className]: className,
        })}
      >
        <Button
          variant="outline"
          className="h-8 w-8 p-0 lg:flex disabled:opacity-50 disabled:bg-muted mr-4"
          onClick={onPrevious}
          disabled={currentPage === 1}
        >
          <RxDoubleArrowLeft className="h-4 w-4" />
        </Button>
        {paginationRange?.map((pageNumber: any, index: number) => {
          if (pageNumber === DOTS) {
            return (
              <li key={index} className="tracking-wider cursor-pointer">
                ...
              </li>
            );
          }

          return (
            <li
              key={index}
              className={classnames(style.pagination_item, {
                "bg-primary/80 text-white font-semibold":
                  isPaginationItemActive(pageNumber),
              })}
              onClick={() => onPageChangeWithRouter(pageNumber)}
            >
              {pageNumber}
            </li>
          );
        })}
        <Button
          variant="outline"
          className="h-8 w-8 p-0 lg:flex disabled:opacity-50 disabled:bg-muted ml-4"
          onClick={onNext}
          disabled={currentPage === lastPage}
        >
          <RxDoubleArrowRight className="h-4 w-4" />
        </Button>
      </ul>
    </div>
  );
};

export default PaginationPage;
