"use client";

import { forwardRef } from "react";

import { cn } from "@/lib/utils";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";

interface FormInputProps {
  id?: string;
  label?: string;
  type?: string;
  placeholder?: string;
  required?: boolean;
  disabled?: boolean;
  errors?: Record<string, string[] | undefined>;
  className?: string;
  onBlur?: () => void;
  onChange?: (e: any) => void;
  value?: string;
}

export const FormInput = forwardRef<HTMLInputElement, FormInputProps>(
  (
    {
      id,
      label,
      type,
      placeholder,
      required,
      className,
      value = "",
      onBlur,
      onChange,
      disabled,
    },
    ref
  ) => {
    return (
      <div className="w-full space-y-2">
        <div className="space-y-2 flex flex-col m-[1px]">
          {label ? (
            <Label htmlFor={id} className="font-normal text-left">
              {label}
            </Label>
          ) : null}
          <Input
            onBlur={onBlur}
            value={value}
            ref={ref}
            required={required}
            id={id}
            placeholder={placeholder}
            type="text"
            className={cn(
              " text-sm px-2 py-2 disabled:bg-slate-100 dark:text-input",
              className
            )}
            aria-describedby={`${id}-error`}
            onChange={onChange}
            disabled={disabled}
          />
        </div>
      </div>
    );
  }
);

FormInput.displayName = "FormInput";
