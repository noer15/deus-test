"use client";
import React, { useState } from "react";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { importData } from "@/lib/api";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";
const ModalImport = () => {
  const router = useRouter();

  const [isOpen, setOpen] = useState<boolean>(false);

  const [file, setFile] = useState<File | null>(null);

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const selectedFile = e.target.files && e.target.files[0];
    setFile(selectedFile || null);
  };

  const handleUpload = async () => {
    if (!file) {
      console.log("No file selected");
      return;
    }
    try {
      await importData(file).then((res) => {
        setFile(null);
        setOpen(false);
        toast.success(res.msg);
        router.push("?success=true");
      });
    } catch (error) {
      console.log(error);
    }
  };

  const resetForm = () => {
    setFile(null);
    setOpen(false);
  };
  return (
    <>
      <Button variant="outline" onClick={() => setOpen(true)}>
        Import
      </Button>
      <Dialog open={isOpen} onOpenChange={resetForm}>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Form Import</DialogTitle>
            <DialogDescription>
              <div className="my-4">
                <div className="space-y-4">
                  <input type="file" onChange={handleFileChange} />
                </div>
                <div className="mt-4 border-t">
                  <div className="pt-4 flex justify-end gap-x-3">
                    <Button type="submit" onClick={handleUpload}>
                      Upload
                    </Button>
                  </div>
                </div>
              </div>
            </DialogDescription>
          </DialogHeader>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default ModalImport;
