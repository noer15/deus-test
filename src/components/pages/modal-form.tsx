"use client";
import React, { useEffect, useState } from "react";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { FormInput } from "../form/form-input";
import { postData, putData } from "@/lib/api";
import { FaEdit } from "react-icons/fa";
import { toast } from "react-hot-toast";
import { useRouter } from "next/navigation";

const ModalForm = ({ props }: any) => {
  const [isOpen, setOpen] = useState<boolean>(false);
  const [id, setId] = useState<null>(null);
  const [name, setName] = useState<string>("");
  const [power, setPower] = useState<string>("");

  const router = useRouter();

  const resetForm = () => {
    setId(null);
    setName("");
    setPower("");
    setOpen(false);
  };

  useEffect(() => {
    if (props) {
      setName(props.name_character);
      setPower(props.strength_power);
      setId(props.id);
    }
  }, [props]);

  const onSave = async () => {
    try {
      if (id === null) {
        await postData({
          name_character: name,
          strength_power: power,
        }).then((res) => {
          resetForm();
          toast.success(res.msg);
          router.push("?success=true");
        });
      } else {
        await putData({
          id,
          name_character: name,
          strength_power: power,
        }).then((res) => {
          resetForm();
          toast.success(res.msg);
          router.push("?success=true");
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {props ? (
        <Button
          variant="outline"
          className="flex h-8 w-8 p-0 data-[state=open]:bg-muted"
          onClick={() => setOpen(true)}
        >
          <FaEdit className="text-gray-400" />
        </Button>
      ) : (
        <Button onClick={() => setOpen(true)}>Tambah</Button>
      )}

      <Dialog open={isOpen} onOpenChange={resetForm}>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Form {props ? "Edit" : "Tambah"}</DialogTitle>
            <DialogDescription>
              <div className="my-4">
                <div className="space-y-4">
                  <FormInput
                    id="name"
                    type="text"
                    label="Name"
                    placeholder="Name Character"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                  <FormInput
                    id="power"
                    label="Power"
                    placeholder="Power"
                    value={power}
                    onChange={(e) => setPower(e.target.value)}
                  />
                </div>
              </div>
              <div className="mt-4 border-t">
                <div className="pt-4 flex justify-end gap-x-3">
                  <Button variant="secondary" onClick={resetForm}>
                    Reset
                  </Button>
                  <Button type="submit" onClick={onSave}>
                    Submit
                  </Button>
                </div>
              </div>
            </DialogDescription>
          </DialogHeader>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default ModalForm;
