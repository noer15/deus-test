import axios from "axios";
const baseURL = process.env.NEXT_PUBLIC_API_KEY;

interface PropsData {
  strength_power?: string;
  name_character?: string;
}

const getData = async ({ page, perPage, search }: Record<string, string>) => {
  try {
    const { data } = await axios.get(`${baseURL}/gamification`, {
      params: {
        page,
        search,
        itemPerPage: perPage,
      },
    });
    return data;
  } catch (error) {
    console.log(error);
  }
};

const postData = async (payload: PropsData) => {
  try {
    const { data } = await axios.post(`${baseURL}/gamification`, payload);
    return data;
  } catch (error) {
    console.log(error);
  }
};

const putData = async (payload: any) => {
  try {
    const { data } = await axios.put(
      `${baseURL}/gamification/${payload.id}`,
      payload
    );
    return data;
  } catch (error) {
    console.log(error);
  }
};

const deleteData = async (id: number) => {
  try {
    const { data } = await axios.delete(`${baseURL}/gamification/${id}`);
    return data;
  } catch (error) {
    console.log(error);
  }
};

const importData = async (payload: any) => {
  try {
    const formData = new FormData();
    formData.append("file", payload);
    const { data } = await axios.post(
      `${baseURL}/uploadGamification`,
      formData
    );
    return data;
  } catch (error) {
    console.log(error);
  }
};

export { getData, postData, importData, deleteData, putData };
