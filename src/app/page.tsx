"use client";

import { useEffect, useState } from "react";
import { getData } from "@/lib/api";
import DataTables from "@/components/datatables";
import ModalForm from "@/components/pages/modal-form";
import ModalImport from "@/components/pages/modal-import";
import PaginationPage from "@/components/pagination/Pagination";
import ModalDelete from "@/components/pages/modal-delete";
import { useRouter, useSearchParams } from "next/navigation";
import { Input } from "@/components/ui/input";

const columns = [
  {
    title: "Name Character",
    key: "name_character",
  },
  {
    title: "Strength Power",
    key: "strength_power",
  },
  {
    title: "Created At",
    key: "created_at",
  },
  {
    title: "Action",
    key: "action",
  },
];

export default function Home() {
  const [currentPage, setCurrentPage] = useState<string>("1");
  const [data, setData] = useState<any>([]);
  const [searchQuery, setSearchQuery] = useState("");

  const router = useRouter();
  const params = useSearchParams();
  const success = params.get("success");

  useEffect(() => {
    const delayDebounceFn: NodeJS.Timeout = setTimeout(() => {
      fetchData();
    }, 500);
    return () => clearTimeout(delayDebounceFn);
  }, [success, currentPage, searchQuery]);

  const fetchData = async () => {
    try {
      const response = await getData({
        page: currentPage,
        perPage: "10",
        search: searchQuery,
      });
      setData(response.data);

      if (success === "true") {
        router.replace("/");
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <main className="container px-6">
      <div className="my-4">
        <div className="flex justify-between">
          <Input
            type="search"
            placeholder="Search"
            className="w-[150px] lg:w-[250px]"
            onChange={(e) => setSearchQuery(e.target.value)}
          />
          <div className="flex gap-x-3 justify-end mb-5">
            <ModalForm />
            <ModalImport />
          </div>
        </div>
        <div className="w-full">
          <DataTables columns={columns} data={data?.data}>
            {({ key, data }) => {
              switch (key) {
                case "action":
                  return (
                    <div className="flex justify-end gap-x-3">
                      <ModalForm props={data} />
                      <ModalDelete props={data.id} />
                    </div>
                  );
                default:
                  return data[key];
              }
            }}
          </DataTables>
          <div className="py-3">
            <PaginationPage
              className="pagination-bar"
              currentPage={currentPage}
              totalCount={100}
              label={data}
              pageSize={10}
              onPageChange={(page: any) => setCurrentPage(page)}
            />
          </div>
        </div>
      </div>
    </main>
  );
}
