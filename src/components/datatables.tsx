"use client";
import React, { ReactElement, useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { RxCaretSort } from "react-icons/rx";

export type DatatablesProps = {
  data: any[] | undefined;
  columns: {
    title: string;
    key: string;
  }[];
  isDetail?: boolean;
  isLoading?: boolean;
  isAction?: boolean;
  onEdit?: (...args: any[]) => any;
  onDetail?: (...args: any[]) => any;
  onDelete?: (...args: any[]) => any;
  children?: (args: { key: string; data?: any }) => ReactElement;
  isChecked?: boolean;
  onDataSelected?: (selectedRows: any[]) => void;
};

const DataTables: React.FC<DatatablesProps> = ({
  data,
  columns,
  children,
  isLoading,
  isChecked = false,
  onDataSelected,
}) => {
  const [sortKey, setSortKey] = useState<string>("");
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");
  const [selectAll, setSelectAll] = useState(false);
  const [selectedRows, setSelectedRows] = useState<any>([]);

  const handleSort = (key: string) => {
    if (sortKey === key) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortKey(key);
      setSortOrder("asc");
    }
  };

  const sortedData = data?.slice().sort((a, b) => {
    const valueA = a[sortKey];
    const valueB = b[sortKey];

    if (valueA < valueB) {
      return sortOrder === "asc" ? -1 : 1;
    }
    if (valueA > valueB) {
      return sortOrder === "asc" ? 1 : -1;
    }
    return 0;
  });

  const toggleSelectAll = () => {
    if (selectAll) {
      setSelectedRows([]);
    } else {
      setSelectedRows(data && data?.map((item: any) => item.id));
    }
    setSelectAll(!selectAll);
  };

  const toggleRowSelect = (rowKey: any) => {
    if (selectAll) {
      // If "Select All" is active, deselect all rows
      setSelectedRows([]);
      setSelectAll(false);
    } else {
      if (selectedRows.includes(rowKey)) {
        setSelectedRows(selectedRows.filter((key: string) => key !== rowKey));
      } else {
        setSelectedRows([...selectedRows, rowKey]);
      }
    }
  };

  useEffect(() => {
    if (onDataSelected) {
      onDataSelected(selectedRows);
    }
  }, [selectedRows, onDataSelected]);

  return (
    <div className="overflow-auto mx-auto w-full">
      {isLoading ? (
        <p>Loading</p>
      ) : (
        <div className="rounded-md border ">
          <Table>
            <TableHeader>
              <TableRow>
                {columns?.map((column, index) => {
                  return (
                    <TableHead
                      key={index}
                      onClick={() => handleSort(column.key)}
                      className={
                        column.key === "action" ? "flex justify-end" : ""
                      }
                    >
                      <div className="flex items-center gap-x-1">
                        <span>{column.title}</span>
                        <RxCaretSort />
                      </div>
                    </TableHead>
                  );
                })}
              </TableRow>
            </TableHeader>
            <TableBody>
              {data && data?.length ? (
                sortedData &&
                sortedData?.map((item, rowIndex) => {
                  const rowKey = item?.id;
                  const isSelected = selectedRows.includes(rowKey);
                  return (
                    <TableRow
                      key={rowKey}
                      className={isSelected ? "bg-muted" : ""}
                    >
                      {columns?.map((column, columnIndex) => {
                        const cellKey = `cell-${rowIndex}-${columnIndex}`;
                        return (
                          <TableCell key={cellKey}>
                            {children
                              ? children({
                                  key: column.key,
                                  data: item,
                                }) != null
                                ? children({
                                    key: column.key,
                                    data: item,
                                  })
                                : "-"
                              : item[column.key] != null
                              ? item[column.key]
                              : "-"}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })
              ) : (
                <TableRow>
                  <TableCell
                    colSpan={columns.length}
                    className="h-24 text-center"
                  >
                    Data tidak ditemukan.
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
      )}
    </div>
  );
};

export default DataTables;
